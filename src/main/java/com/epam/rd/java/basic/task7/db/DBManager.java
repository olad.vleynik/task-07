package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;

	private static final String PROPERTIES_FILE = "app.properties";
	private static String connectionURL;

	private static final String INSERT_USER_QUERY = "INSERT INTO users VALUES (DEFAULT, ?)";
	private static final String ALL_USERS_QUERY = "SELECT * FROM users ORDER BY id";
	private static final String INSERT_TEAM_QUERY = "INSERT INTO teams VALUES (DEFAULT, ?)";
	private static final String ALL_TEAMS_QUERY = "SELECT * FROM teams ORDER BY id";
	private static final String SELECT_USER_QUERY = "SELECT * FROM users WHERE login=?";
	private static final String SELECT_TEAM_QUERY = "SELECT * FROM teams WHERE name=?";
	private static final String INSERT_USERS_TEAMS_QUERY = "INSERT INTO users_teams VALUES (?, ?)";
	private static final String DELETE_USERS_TEAMS_BY_USERID_QUERY = "DELETE FROM users_teams WHERE user_id=?";
	private static final String ALL_USER_TEAMS_QUERY = "SELECT id, name FROM teams INNER JOIN users_teams ut on teams.id = ut.team_id WHERE user_id=?";
	private static final String DELETE_TEAM_QUERY = "DELETE FROM teams WHERE id=?";
	private static final String UPDATE_TEAM_QUERY = "UPDATE teams SET name=? WHERE id=?";
	private static final String DELETE_USER_QUERY = "DELETE FROM users WHERE id=?";

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();

			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				Properties DBProperties = new Properties();
				DBProperties.load(new FileInputStream(PROPERTIES_FILE));
				connectionURL = DBProperties.getProperty("connection.url");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		try (Connection connection = getConn()) {
			Statement statement = connection.createStatement();
			ResultSet users = statement.executeQuery(ALL_USERS_QUERY);
			List<User> usersList = new ArrayList<>();

			while (users.next()) {
				User user = User.createUser(users.getString("login"));
				user.setId(users.getInt("id"));
				usersList.add(user);
			}

			return usersList;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean insertUser(User user) throws DBException {
		try (Connection connection = getConn()) {
			PreparedStatement statement = connection.prepareStatement(INSERT_USER_QUERY, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, user.getLogin());
			statement.executeUpdate();

			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
				user.setId(rs.getInt(1));
			}
			return true;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		try (Connection connection = getConn()) {
			for (int i = 0; i < users.length; i++) {
				PreparedStatement deleteFromTeamStatement = connection.prepareStatement(DELETE_USER_QUERY);
				deleteFromTeamStatement.setInt(1, users[i].getId());
				deleteFromTeamStatement.executeUpdate();
			}

			return true;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
	}

	public User getUser(String login) throws DBException {
		try (Connection connection = getConn()) {
			PreparedStatement statement = connection.prepareStatement(SELECT_USER_QUERY);
			statement.setString(1, login);
			ResultSet setOfUser = statement.executeQuery();
			setOfUser.next();

			User user = User.createUser(setOfUser.getString("login"));
			user.setId(setOfUser.getInt("id"));

			return user;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
	}

	public Team getTeam(String name) throws DBException {
		try (Connection connection = getConn()) {
			PreparedStatement statement = connection.prepareStatement(SELECT_TEAM_QUERY);
			statement.setString(1, name);
			ResultSet setOfTeam = statement.executeQuery();
			setOfTeam.next();

			Team team = Team.createTeam(setOfTeam.getString("name"));
			team.setId(setOfTeam.getInt("id"));

			return team;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
	}

	public List<Team> findAllTeams() throws DBException {
		try (Connection connection = getConn()) {
			Statement statement = connection.createStatement();
			ResultSet teams = statement.executeQuery(ALL_TEAMS_QUERY);
			List<Team> teamsList = new ArrayList<>();

			while (teams.next()) {
				Team team = Team.createTeam(teams.getString("name"));
				team.setId(teams.getInt("id"));
				teamsList.add(team);
			}

			return teamsList;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean insertTeam(Team team) throws DBException {
		try (Connection connection = getConn()) {
			PreparedStatement statement = connection.prepareStatement(INSERT_TEAM_QUERY, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, team.getName());
			statement.executeUpdate();

			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
				team.setId(rs.getInt(1));
			}
			return true;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection = getConn();
		try {
			connection.setAutoCommit(false);

			//PreparedStatement delStatement = connection.prepareStatement(DELETE_USERS_TEAMS_BY_USERID_QUERY);
			//delStatement.setInt(1, user.getId());
			//delStatement.executeUpdate();

			for (int i = 0; i < teams.length; i++) {
				PreparedStatement insertStatement = connection.prepareStatement(INSERT_USERS_TEAMS_QUERY);
				insertStatement.setInt(1, user.getId());
				insertStatement.setInt(2, teams[i].getId());
				insertStatement.executeUpdate();
			}

			connection.commit();
			connection.close();
			return true;
		} catch (SQLException e) {
			try {
				connection.rollback();
				connection.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		try (Connection connection = getConn()) {
			PreparedStatement statement = connection.prepareStatement(ALL_USER_TEAMS_QUERY);
			statement.setInt(1, user.getId());
			ResultSet teams = statement.executeQuery();
			List<Team> teamsList = new ArrayList<>();

			while (teams.next()) {
				Team team = Team.createTeam(teams.getString("name"));
				team.setId(teams.getInt("id"));
				teamsList.add(team);
			}

			return teamsList;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean deleteTeam(Team team) throws DBException {
		try (Connection connection = getConn()) {
			PreparedStatement deleteFromTeamStatement = connection.prepareStatement(DELETE_TEAM_QUERY);
			deleteFromTeamStatement.setInt(1, team.getId());
			deleteFromTeamStatement.executeUpdate();

			return true;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		try (Connection connection = getConn()) {
			PreparedStatement statement = connection.prepareStatement(UPDATE_TEAM_QUERY);
			statement.setString(1, team.getName());
			statement.setInt(2, team.getId());
			statement.executeUpdate();

			return true;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
	}

	private static Connection getConn() {
		try {
			return DriverManager.getConnection(connectionURL);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
